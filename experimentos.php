<?php
$dir =  __DIR__  . '/experimentos';
$files1 = scandir($dir);
$json = array();
foreach ($files1 as $file => $value){
  $filedir = $dir . '/' . $value;
  if(!is_file($filedir) && $value != '..' && $value != '.'){
    $input = $filedir . '/input.txt';
    $fh = fopen($input,'r');
    $txt = '';
    while ($line = fgets($fh)) {
      // <... Do your work with the line ...>
      $txt=$txt.$line;
    }
    fclose($fh);
    $txtJson = json_decode($txt, true);
    array_push($json, $txtJson);
  }
}
echo json_encode($json, JSON_FORCE_OBJECT);

 ?>
