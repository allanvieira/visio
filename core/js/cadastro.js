var experimentos,
$tbody = $('#experimentos tbody'),
$btn_novo = $('#btn-novo'),
$modal_novo = $('#modal-novo'),
$modal_novo_body = $('#modal-novo .modal-body'),
$submit_novo = $('#modal-novo .submit'),
$modal_editar = $('#modal-editar'),
$modal_editar_body = $('#modal-editar .modal-body'),
$submit_editar = $('#modal-editar .submit'),
$submit_excluir = $('#modal-editar .excluir'),
$modal_formulario = $('#modal-formulario'),
$modal_formulario_body = $('#modal-formulario .modal-body'),
$submit_formulario = $('#modal-formulario .submit');

initialize = function(){
  $.get("experimentos.php", function(data) {
    experimentos = JSON.parse(data);
    for(e in experimentos){
      exp = experimentos[e];
      console.log(exp);
      html = '<tr><td>'+exp.id+'</td><td>'+exp.name+'</td><td>'+exp.id_vimeo+'</td><td><a href="'+exp.link_vimeo+'" target="_blank">Link</a></td><td>'+exp.desc+'</td><td><a href="./?experimento='+exp.id+'" target="_blank">Link</a></td><td><button type="button" class="btn btn-default btn-xs btn-formulario" data-exp-id="'+exp.id+'" >Formulário</button></td><td><button type="button" class="btn btn-default btn-xs btn-editar" data-exp-id="'+exp.id+'" >Editar</button></td></tr>';
      $tbody.append(html);
    }
  });

}

$modal_novo.on('shown.bs.modal', function () {
  html = ''
  html += '<form id="form_novo" action="./novoexperimento.php">';
  html += '<div class="form-group">';
  html += '<label for="exp_id">ID</label>';
  html += '<input type="text" class="form-control" id="exp_id" placeholder="ID">'
  html += '<p class="help-block">Sem espaços, será utilizado no link do experimento.</p>'
  html += '</div>'
  html += '<label for="exp_name">Nome</label>';
  html += '<input type="text" class="form-control" id="exp_name" placeholder="Nome">'
  html += '</div>'
  html += '</div>'
  html += '<label for="exp_id_vimeo">ID Vídeo Vimeo</label>';
  html += '<input type="text" class="form-control" id="exp_id_vimeo" placeholder="Id Vimeo">'
  html += '<p class="help-block">Somente o ID do vídeo do Vimeo, a url do video será montada automaticamente.</p>'
  html += '</div>'
  html += '<label for="exp_id_vimeo">Descrição</label>';
  html += '<textarea class="form-control" id="exp_id_desc" placeholder="Descrição"></textarea>'
  html += '</div>'
  html += '</form>'
  $modal_novo_body.html(html)
})

$submit_novo.on('click',function(){
  params = {};
  exp_json = {};
  exp_json.id = $('#exp_id').val();
  exp_json.name = $('#exp_name').val();
  exp_json.id_vimeo = $('#exp_id_vimeo').val();
  exp_json.link_vimeo = 'https://player.vimeo.com/video/'+$('#exp_id_vimeo').val()+'?api=1&player_id=player1'
  exp_json.desc = $('#exp_id_desc').val();
  params.exp_id = $('#exp_id').val()
  params.exp_json = JSON.stringify(exp_json)
  console.log(JSON.stringify(exp_json));

  $.get('./novoexperimento.php', params , function(data) {
    $tbody.html('');
    initialize();
  });
});

$(document).on('click','#experimentos tbody .btn-editar',function(){
  params = {};
  params.action = 'exibir';
  params.id = $(this).attr('data-exp-id');
  $.get('./editarexperimento.php', params , function(data) {
    experimento = JSON.parse(data);
    html = ''
    html += '<form id="form_novo" action="./novoexperimento.php">';
    html += '<div class="form-group">';
    html += '<label for="exp_id">ID</label>';
    html += '<input type="text" class="form-control" id="exp_id" placeholder="ID" value="'+experimento.id+'" data-old-id="'+experimento.id+'">'
    html += '<p class="help-block">Sem espaços, será utilizado no link do experimento.</p>'
    html += '</div>'
    html += '<label for="exp_name">Nome</label>';
    html += '<input type="text" class="form-control" id="exp_name" placeholder="Nome" value="'+experimento.name+'">'
    html += '</div>'
    html += '</div>'
    html += '<label for="exp_id_vimeo">ID Vídeo Vimeo</label>';
    html += '<input type="text" class="form-control" id="exp_id_vimeo" placeholder="Id Vimeo" value="'+experimento.id_vimeo+'">'
    html += '<p class="help-block">Somente o ID do vídeo do Vimeo, a url do video será montada automaticamente.</p>'
    html += '</div>'
    html += '<label for="exp_id_vimeo">Descrição</label>';
    html += '<textarea class="form-control" id="exp_id_desc" placeholder="Descrição">'+experimento.desc+'</textarea>'
    html += '</div>'
    html += '</form>'
    $modal_editar_body.html(html)
  });
  $modal_editar.modal('toggle')
});

$submit_excluir.on('click',function(){
  params = {};
  params.action = 'excluir';
  params.id = $('#modal-editar #exp_id').val();
  $.get('./editarexperimento.php', params , function(data) {
    $tbody.html('');
    initialize();
  });
});

$submit_editar.on('click',function(){
  exp_json = {};
  exp_json.id = $('#exp_id').val();
  exp_json.name = $('#exp_name').val();
  exp_json.id_vimeo = $('#exp_id_vimeo').val();
  exp_json.link_vimeo = 'https://player.vimeo.com/video/'+$('#exp_id_vimeo').val()+'?api=1&player_id=player1'
  exp_json.desc = $('#exp_id_desc').val();
  params = {};
  params.action = 'editar';
  params.id = $('#modal-editar #exp_id').val();
  params.oldid = $('#modal-editar #exp_id').attr('data-old-id');
  params.json = JSON.stringify(exp_json)
  console.log(params);
  $.get('./editarexperimento.php', params , function(data) {
    $tbody.html('');
    initialize();
  });
});


$(document).on('click','#experimentos tbody .btn-formulario',function(){
  template_id = $(this).attr('data-exp-id');
  $.getJSON("experimentos/"+template_id+"/formulario.txt", function(data) {
    html = geraFormulario(data,template_id);
    $modal_formulario_body.html(html);
    $modal_formulario.modal('toggle');
  });

});

geraFormulario = function(formJson,template_id){

  html = ''
  html += '<button type="button" class="btn btn-default btn-xs btn-add-input">Adicionar</button>'
  html += '<input type="hidden" class="btn btn-default btn-xs id-template" value="'+template_id+'"/>'
  for(f in formJson){
    form = formJson[f];
    html+='<div class="panel panel-default panel-form"> <div class="panel-body">';
    html+='<div class="row">'
    html+='<div class="col-md-6">';
    html+='<div class="form-group">'
    html+='<label>ID:</label>';
    html+='<input type="text" class="form-control form-id" value="'+form.id+'"/>';
    html+='</div>'
    html+='</div>';
    html+='<div class="col-md-6">';
    html+='<div class="form-group">'
    html+='<label>Descrição:</label>';
    html+='<input type="text" class="form-control form-descricao" value="'+form.descricao+'"/>';
    html+='</div>'
    html+='</div>';
    html+='</div>'

    html+='<div class="row">';
    html+='<div class="col-md-6">';
    html+='<label>Tipo:</label>';
    html+='<select class="form-control form-tipo">';
    if(form.tipo == 'texto'){
      html+= '<option value="texto" selected="true">Texto</option>'
      html+= '<option value="opcao">Opção</option>'
    }else{
      html+= '<option value="texto">Texto</option>'
      html+= '<option value="opcao" selected="true">Opção</option>'
    }
    html+='</select>';
    html+='</div>';
    html+='<div class="col-md-6">';
    html+='<button type="button" class="btn btn-default btn-xs btn-del-input">Remover</button>'
    html+='</div>';
    html+='</div>';
    if(form.tipo == 'opcao'){
      html+='<div class="panel panel-default panel-opcao"> <div class="panel-body">';
      html+= '<button type="button" class="btn btn-default btn-xs btn-add-opcao">Adicionar</button>'
      for(o in form.opcoes){
        opcao = form.opcoes[o];
        html+='<div class="row">'
        html+='<div class="col-md-4">';
        html+='<div class="form-group">'
        html+='<label>ID:</label>';
        html+='<input type="text" class="form-control form-opcao-id" value="'+opcao.id+'"/>';
        html+='</div>'
        html+='</div>';
        html+='<div class="col-md-4">';
        html+='<div class="form-group">'
        html+='<label>Descrição:</label>';
        html+='<input type="text" class="form-control form-opcao-descricao" value="'+opcao.descricao+'"/>';
        html+='</div>'
        html+='</div>';
        html+='<div class="col-md-4"><button type="button" class="btn btn-default btn-xs btn-del-opcao">Remover</button></div>';
        html+='</div>'
      }

      html+='</div>';
    }
    html += '</div></div>'
  }
  return html;
}

$(document).on('click','.btn-add-input',function(){
  html='';
  html+='<div class="panel panel-default panel-form"> <div class="panel-body">';
  html+='<div class="row">'
  html+='<div class="col-md-6">';
  html+='<div class="form-group">'
  html+='<label>ID:</label>';
  html+='<input type="text" class="form-control form-id" value="novo"/>';
  html+='</div>'
  html+='</div>';
  html+='<div class="col-md-6">';
  html+='<div class="form-group">'
  html+='<label>Descrição:</label>';
  html+='<input type="text" class="form-control form-descricao" value="Novo"/>';
  html+='</div>'
  html+='</div>';
  html+='</div>'

  html+='<div class="row">';
  html+='<div class="col-md-6">';
  html+='<label>Tipo:</label>';
  html+='<select class="form-control form-tipo">';
  html+= '<option value="texto" selected="true">Texto</option>'
  html+= '<option value="opcao">Opção</option>'
  html+='</select>';
  html+='</div>';
  html+='<div class="col-md-6">';
  html+='<button type="button" class="btn btn-default btn-xs btn-del-input">Remover</button>'
  html+='</div>';
  html+='</div>';
  html += '</div></div>';

  $modal_formulario_body.append(html);
});

$(document).on('click','.btn-del-input',function(){
  $parent = $(this).parent().parent().parent().parent();
  $parent.remove();
});

$(document).on('change','.form-tipo',function(){
  $parent = $(this).parent().parent().parent();
  value = $('option:selected',this).val();
  if(value == 'texto'){
    $('.panel-opcao',$parent).remove();
  }else{
    html = '';
    html+='<div class="panel panel-default panel-opcao"> <div class="panel-body">';
    html+= '<button type="button" class="btn btn-default btn-xs btn-add-opcao">Adicionar</button>'
    html+='<div class="row">'
    html+='<div class="col-md-4">';
    html+='<div class="form-group">'
    html+='<label>ID:</label>';
    html+='<input type="text" class="form-control form-opcao-id" value="1"/>';
    html+='</div>'
    html+='</div>';
    html+='<div class="col-md-4">';
    html+='<div class="form-group">'
    html+='<label>Descrição:</label>';
    html+='<input type="text" class="form-control form-opcao-descricao" value="Uma"/>';
    html+='</div>'
    html+='</div>';
    html+='<div class="col-md-4"><button type="button" class="btn btn-default btn-xs btn-del-opcao">Remover</button></div>';
    html+='</div>'
    html+='</div>'
    $parent.append(html);
  }
});

$(document).on('click','.btn-add-opcao',function(){
  $parent = $(this).parent();
  html = '';
  html+='<div class="row">'
  html+='<div class="col-md-4">';
  html+='<div class="form-group">'
  html+='<label>ID:</label>';
  html+='<input type="text" class="form-control form-opcao-id" value="1"/>';
  html+='</div>'
  html+='</div>';
  html+='<div class="col-md-4">';
  html+='<div class="form-group">'
  html+='<label>Descrição:</label>';
  html+='<input type="text" class="form-control form-opcao-descricao" value="Uma"/>';
  html+='</div>'
  html+='</div>';
  html+='<div class="col-md-4"><button type="button" class="btn btn-default btn-xs btn-del-opcao">Remover</button></div>';
  html+='</div>'
  $parent.append(html);
});

$(document).on('click','.btn-del-opcao',function(){
  $parent = $(this).parent().parent();
  $parent.remove();
});

$submit_formulario.on('click',function(){
  params = {}
  params.id = $('.id-template',$modal_formulario_body).val();
  formJson = [];

  $('.panel-form').each(function(){
    form = {};
    form.id = $('.form-id',this).val();
    form.descricao = $('.form-descricao',this).val();
    form.tipo = $('.form-tipo option:selected',this).val();
    if(form.tipo == 'opcao'){
      form.opcoes = [];
      $('.panel-opcao .row',this).each(function(){
        opcao = {};
        opcao.id = $('.form-opcao-id',this).val();
        opcao.descricao = $('.form-opcao-descricao',this).val();
        form.opcoes.push(opcao)
      });
    }
    formJson.push(form)
  });

  params.json = JSON.stringify(formJson);
  $.get('./editarformulario.php', params , function(data) {
  });


})
