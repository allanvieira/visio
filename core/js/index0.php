<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Strict//EN">
<html>
<head>
  <title>Online PW VIP</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link type="text/css" href="core/css/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
  <style type="text/css"> @import "core/css/PWVIP.css";</style>
  <style type="text/css"> @import "core/css/bootstrap.min.css";</style>
  <script type="text/javascript" src="core/js/jquery-1.7.1.min.js"></script>
  <script type="text/javascript" src="core/js/jquery-ui-1.8.18.custom.min.js"></script>
  <script type="text/javascript">
  var ua = navigator.userAgent.toLowerCase();
  var uMobile = '';

  uMobile += 'iphone;ipod;windows phone;android;iemobile 8;meego;ipad';
  v_uMobile = uMobile.split(';');

  var boolMovel = false;
  for (i=0;i<=v_uMobile.length;i++){
    if (ua.indexOf(v_uMobile[i]) != -1){
      boolMovel = true;
    }
  }
  var mHeight = screen.height;
  $(document).ready(function(){
    if (boolMovel == true){
      mHeight -= 115;
      $('body').removeClass('defaultBody');
      $('body').addClass('mobileLayout');
    }
  });

  </script>


</head>
<body>

  <div id="nav-header">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">
            <img alt="Brand" src="core/logo.png">
          </a>
        </div>
      </div>
    </nav>
  </div>

  <div id="content" class="container">
    <noscript>
    <div id="instructions">
      O Javascript deve estar ativo para esta aplicação funcionar.<br><br>
      Ative o Javascript para continuar.<br><br>
    </div>
    </noscript>
  </div>
  <script type="text/javascript" src="core/js/gumadapter.js"></script>
  <script type="text/javascript" src="core/js/RecordRTC.js"></script>
  <script type="text/javascript" src="core/js/PWVIP.js" charset="utf-8"></script>
  <script type="text/javascript">initialize()</script>
</body>
</html>
