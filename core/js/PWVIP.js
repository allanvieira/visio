var template,
    userInfo={},
    sub;

var Froogaloop=function(){function e(a){return new e.fn.init(a)}function g(a,c,b){if(!b.contentWindow.postMessage)return!1;a=JSON.stringify({method:a,value:c});b.contentWindow.postMessage(a,h)}function l(a){var c,b;try{c=JSON.parse(a.data),b=c.event||c.method}catch(e){}"ready"!=b||k||(k=!0);if(!/^https?:\/\/player.vimeo.com/.test(a.origin))return!1;"*"===h&&(h=a.origin);a=c.value;var m=c.data,f=""===f?null:c.player_id;c=f?d[f][b]:d[b];b=[];if(!c)return!1;void 0!==a&&b.push(a);m&&b.push(m);f&&b.push(f);
return 0<b.length?c.apply(null,b):c.call()}function n(a,c,b){b?(d[b]||(d[b]={}),d[b][a]=c):d[a]=c}var d={},k=!1,h="*";e.fn=e.prototype={element:null,init:function(a){"string"===typeof a&&(a=document.getElementById(a));this.element=a;return this},api:function(a,c){if(!this.element||!a)return!1;var b=this.element,d=""!==b.id?b.id:null,e=c&&c.constructor&&c.call&&c.apply?null:c,f=c&&c.constructor&&c.call&&c.apply?c:null;f&&n(a,f,d);g(a,e,b);return this},addEvent:function(a,c){if(!this.element)return!1;
var b=this.element,d=""!==b.id?b.id:null;n(a,c,d);"ready"!=a?g("addEventListener",a,b):"ready"==a&&k&&c.call(null,d);return this},removeEvent:function(a){if(!this.element)return!1;var c=this.element,b=""!==c.id?c.id:null;a:{if(b&&d[b]){if(!d[b][a]){b=!1;break a}d[b][a]=null}else{if(!d[a]){b=!1;break a}d[a]=null}b=!0}"ready"!=a&&b&&g("removeEventListener",a,c)}};e.fn.init.prototype=e.fn;window.addEventListener?window.addEventListener("message",l,!1):window.attachEvent("onmessage",l);return window.Froogaloop=
window.$f=e}();

//funcao captura os parametros da url
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function randomString(length) {
	var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i) result += chars.charAt(Math.floor(Math.random() * (chars.length - 1)));
    return result;
}

//Objeto Player
var Player = function(){

  this.status = 'instance';

  this.init = function(frame,video,recorder){

    this.frame = frame;
    this.frame.height = screen.height -140;
    this.frame.width = screen.width -15;
    this.frame.src = video;
    this.recorder = recorder;
    this.player = $f(this.frame);
    this.status = 'load';
    this.load = 0;
    console.log(this.status);
    this.player.addEvent('ready',function(){
      this.status = 'ready';
      console.log('player : ' + this.status);
      this.player.addEvent('pause',function(id){
        this.status = 'Pause'
        console.log('player : ' + this.status);
        //this.recorder.pause();
      }.bind(this));
      this.player.addEvent('finish',function(id){
        this.status = 'Finish';
        console.log('player : ' + this.status);
        this.recorder.stop();
      }.bind(this));
      this.player.addEvent('playProgress',function(data,id){

          if (this.recorder.status == 'pause'){
            this.recorder.resume();
          }else if (this.recorder.status == 'ready'){
            this.recorder.start();
          }
          this.status = data.seconds + 's played';
          console.log('player : ' + this.status);


      }.bind(this));
      this.player.addEvent('loadProgress',function(data,id){
        /*if (data.percent < 0.50){
          console.log(data);
          if(this.status != 'pause'){
              this.pause();
              this.status = 'pause'
              console.log('player : ' + this.status);
          }
        }else{
          if(this.recorder.status != 'start'){
            this.start();
            this.recorder.start();
          }
        }*/
      }.bind(this));
    }.bind(this));
  }

  this.start = function(){
      //this.setFullScreen();
      this.player.api('play');
  };

  this.pause = function(){
      this.player.api('pause')
  };

  this.goto = function(go){
    this.player.api('seekTo',go)
  }

  this.setFullScreen = function(){
    try{
      this.player.api('requestFullscreen')
    }catch(error){
        try{
          this.player.api('mozRequestFullScreen')
        }catch(error){
          try{
            this.player.api('webkitRequestFullscreen')
          }catch(error){
            console.log('nao deu');
          }
        }
    }
  }

}

//Objeto Recorder
var Recorder = function(){
  this.mediaConstraints = {video: true};
  this.options = {
    type: 'video',
    frameInterval: 20 // minimum time between pushing frames to Whammy (in milliseconds)
  };
  this.status = 'load';
  this.checkPlayerVideo = function(){
    if(this.playerVideo.status=='ready'){
        this.playerVideo.start();
    }else{
      setTimeout(function(){this.checkPlayerVideo()}.bind(this), 1000);
    }
  }
  this.showImage = function(player){


  }
  this.init = function(playerVideo,player){
    this.player = player;
    this.player.width = screen.width * 0.8
    this.player.height = screen.height -350
    this.playerVideo = playerVideo;
    navigator.mediaDevices.getUserMedia(this.mediaConstraints).then(function successCallback(mediaStream){
      this.player.src = URL.createObjectURL(mediaStream);
      this.player.muted = true;
      this.player.play();
      loadInstructions('four');
      this.recordRTC = RecordRTC(mediaStream, this.options);
      this.status = 'ready';

    }.bind(this)).catch(function errorCallback(error){
      console.log(error)
      alert(error)
    });
  }

  this.start = function(){
    this.recordRTC.startRecording();
    this.status = 'start'
    console.log('recoreder : ' + this.status);
  }

  this.stop = function(){
    this.status = 'finish'
    loadInstructions('upload');
    console.log('recorder : ' + this.status);
  }

  this.startBlob = function(){
    this.recordRTC.stopRecording(function(videoURL) {
        var blob = this.recordRTC.blob;
        var url = URL.createObjectURL(blob);

        //this.player.src = url;
        //this.player.muted = false;
        //this.status = 'stop'
        this.upload(blob,sub);
        //this.download(sub,url);

        this.recordRTC.getDataURL(function(dataURL){
          //window.open(dataURL);
          //this.download('teste',dataURL);
        }.bind(this));
    }.bind(this));
  }

  this.pause = function(){
    this.status = 'pause'
    this.recordRTC.pauseRecording();
    console.log('recorder : ' + this.status);
  }

  this.resume = function(){
    this.status = 'resume'
    this.recordRTC.resumeRecording();
    console.log('recorder : ' + this.status);
  }

  this.download = function(filename,url){
    $(".progress-bar").css('width','100%');
    $(".progress-bar").attr('aria-valuenow',100);
    $(".progress-bar span").text('100% Completo');

      setTimeout(function(){
        loadInstructions('finish');
      }, 2000);

    var a = document.createElement('a');
    a.href = url;
    a.download = filename+'.mp4'; // Set the file name.
    a.style.display = 'none';
    document.body.appendChild(a);
    a.click();
    delete a;
  }

  this.upload = function(blob,filename){
    //apura informacoes
    var str = "";
	  var columnsName = "";
	  var strUserInfo = "";
	  var d = new Date();

	  var subject = sub;
    subject = subject.length==0 ? "unknown" : subject;

    var replyDate = d.getDate() < 10 ? ('0'+ d.getDate()) : d.getDate();
	  replyDate += "/";
	  replyDate += (d.getMonth() +1) < 10 ? ('0'+ (d.getMonth() +1)) : (d.getMonth() +1);
	  replyDate += "/";
	  replyDate += d.getFullYear();

	  var replyHour = d.getHours() < 10 ? ('0' + d.getHours()) : d.getHours();
	  replyHour += ":";
	  replyHour +=  d.getMinutes() < 10 ? ('0' + d.getMinutes()) : d.getMinutes();
	  replyHour += ":";
	  replyHour += d.getSeconds() < 10 ? ('0' + d.getSeconds()) : d.getSeconds();

	  var columnsName = "data,hora,resposta";

    for(var info in userInfo){
		    columnsName += ","+info
		    if(strUserInfo.length > 0){
			       strUserInfo += ","+userInfo[info];
		    }else{
		        strUserInfo += userInfo[info];
		    }
	  }
	  columnsName += "\n";
    str += replyDate + ",";
		str += replyHour + ",";
		str += subject + ",";
		str += strUserInfo +"\n"

    filename+='.wmv'

    // create FormData
    var formData = new FormData();
    formData.append('filename', filename.toString());
    formData.append('experimenter',template.id.toString())
    formData.append('template',template.id.toString())
    formData.append('data',str.toString())
    formData.append('columnsName',columnsName.toString())
    formData.append('blob', blob);

    this.xhr('core/save.php', formData, function(progress) {
      /*if (progress !== 'upload-ended') {
        callback(progress);
        return;
      }
      var initialURL = location.href.replace(location.href.split('/').pop(), '') + 'uploads/';
      callback('ended', initialURL + fileName);
      // to make sure we can delete as soon as visitor leaves
      listOfFilesUploaded.push(initialURL + fileName);*/
      try{
        progress = progress * 1;
        $(".progress-bar").css('width',progress+'%');
        $(".progress-bar").attr('aria-valuenow',progress);
        $(".progress-bar span").text(progress + '% Completo');
        if (progress == 100){
          setTimeout(function(){
            loadInstructions('finish');
          }, 2000);
        }
      }catch(error){
        console.log(progress);
      }

    });
  }

  this.xhr = function(url, data, callback) {
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (request.readyState == 4 && request.status == 200) {
            callback(location.href + request.responseText);
        }
    };
    request.upload.onloadstart = function() {
      callback('Upload started...');
    };
    request.upload.onprogress = function(event) {
      callback(Math.round(event.loaded / event.total * 100));
    };
    request.upload.onload = function() {
      callback('progress-about-to-end');
    };
    request.upload.onload = function() {
      callback('progress-ended');
    };
    request.upload.onerror = function(error) {
      callback('Failed to upload to server');
      console.error('XMLHttpRequest failed', error);
    };
    request.upload.onabort = function(error) {
      callback('Upload aborted.');
      console.error('XMLHttpRequest aborted', error);
    };
    request.open('POST', url);
    request.send(data);

  }

}

var defaultPlayer = new Player();
var defaultRecorder = new Recorder();


// Inicia experimento ou tela inicial
function initialize()
{


  //procura parametros na url
  if(document.location.toString().indexOf('?') > -1){
    //procura parametro experimento
    if(document.location.toString().indexOf('experimento=') > -1){
      template = getParameterByName('experimento');
      $.getJSON("experimentos/"+template+"/input.txt", function(data) {
        template=data;
        template.id = getParameterByName('experimento');
        /*$.get("core/instruct0.html", function(data) {
				  $("#content").html(data);
  				$("#subID").val(randomString(10));
			  });*/
        $.getJSON("experimentos/"+template.id+"/formulario.txt", function(data) {
          formulario = geraFormulario(data)
          $("#content").html(formulario);
          $("#subID").val(randomString(10));
        });
		  });
    }
  }


}

function loadInstructions(stage){
  switch(stage)
	{
		case 'one':
      currentState = "instruction";
      sub = $("#subID").val();

      userInfo['nome'] = ($('#nome').val() == '' ? 'desconhecido' : $('#nome').val())
      $('.userInfo').each(function(){
        columnName = $(this).attr('data-column');
        value = $(this).val();
        userInfo[columnName] = value;
      });
      if(sub.search('/[^a-zA-Z0-9]/g')==-1)
			{
				$.get("core/instruct1.html", function(data) {
					$("#content").html(data);
					$(".PWVIPname").html(template.name);
          $(".PWVIPdesc").html(template.desc);
				});
			}
			else
			{
				alert("Por favor, insira um ID válido");
			}
      break
    case 'two':
      currentState = "web-cam";
      $.get("core/instruct2.html", function(data) {
        $("#content").html(data);
      });
      break
    case 'tree':
        currentState = "web-cam";

        $.get("core/instruct3.html", function(data) {
          $("#content").html(data);
          defaultRecorder.init(defaultPlayer,document.querySelector('#webcamplayer'));
        });

        break
    case 'start':
      $('#webcamplayer').remove()
      $("#content").hide()
      $.get("experimentos/modelo.html", function(data) {
        $("body").append(data);
        defaultPlayer.init($('#player1')[0],template.link_vimeo,defaultRecorder);
        defaultRecorder.checkPlayerVideo();
      });
      break
    case 'upload':
      $('#experimenter').remove();
      $.get("core/instruct4.html", function(data) {
        $("#content").html(data);
        $("#content").show();
        defaultRecorder.startBlob();
      });
      break
    case 'finish':
      if(urlRedirect != ''){
        document.location = urlRedirect
      }else{
        $.get("core/instruct5.html", function(data) {
          $("#content").html(data);
        });
      }
      break
  }
}

geraFormulario = function(formJson){
  html = '<div class="container">'
  html += '<div class="form-group"><label for="subID">ID: </label><input type="text" id="subID" name="subID" class="form-control disabled" disabled="disabled"/></div>';
  for(f in formJson){
    form = formJson[f];
    html += '<div class="form-group">';
    html += '<label for="'+form.id+'">'+form.descricao+': </label>';
    if(form.tipo == 'texto'){
      html+='<input type="text" id="'+form.id+'" name="'+form.id+'" class="form-control userfInfo" data-columns="'+form.id+'"/>';
    }else{
      html+='<select class="form-control userInfo" data-column="'+form.id+'">';
      for(o in form.opcoes){
        opcao = form.opcoes[o];
        html+= '<option value="'+opcao.id+'">'+opcao.descricao+'</option>'
      }
      html+='</select>';
    }
    html += '</div>';
  }
  html += '</div>	<input type="submit" value="Iniciar Experimento" onclick=\'loadInstructions("one");\' class="btn btn-default">'
  return html;
}
/*
//carrega pagina com o video
$.get("experimentos/modelo.html", function(data) {
      $("body").append(data);
      defaultPlayer.init($('#player1')[0],EXPERIMENTODATA.video,defaultRecorder);
      defaultRecorder.init(defaultPlayer);
});*/
