<?php
  if( isset($_REQUEST['exp_id']) ){
    $exp_id = $_REQUEST['exp_id'];
    $exp_json = $_REQUEST['exp_json'];
    $dir =  __DIR__  . '/experimentos/'.$exp_id;
    if (!file_exists($dir)) {
      mkdir($dir, 0777, true);
      mkdir($dir.'/uploads', 0777, true);
      $file = fopen($dir."/input.txt","wb");
      fwrite($file,$exp_json);
      fclose($file);
      $fileform = fopen($dir."/formulario.txt","wb");
      fwrite($fileform,'[{ "id":"nome","descricao": "Nome","tipo":"texto"},{"id":"sexo","descricao": "Sexo","tipo": "opcao","opcoes":[{"id":"M","descricao":"Masculino"},{"id":"F","descricao":"Feminino"}]}]');
      fclose($fileform);
    }
  }
 ?>
