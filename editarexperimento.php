<?php
function rrmdir($dir) {
   if (is_dir($dir)) {
     $objects = scandir($dir);
     foreach ($objects as $object) {
       if ($object != "." && $object != "..") {
         if (is_dir($dir."/".$object))
           rrmdir($dir."/".$object);
         else
           unlink($dir."/".$object);
       }
     }
     rmdir($dir);
   }
 }

$dir =  __DIR__  . '/experimentos/';
if( isset($_REQUEST['action']) ){
  if($_REQUEST['action'] == 'exibir'){
    $input = $dir . $_REQUEST['id'] . '/input.txt';
    $fh = fopen($input,'r');
    $txt = '';
    while ($line = fgets($fh)) {
      $txt=$txt.$line;
    }
    fclose($fh);
    $txtJson = json_decode($txt, true);
    echo json_encode($txtJson, JSON_FORCE_OBJECT);;
  }elseif ($_REQUEST['action'] == 'excluir') {
    rrmdir($dir . $_REQUEST['id']);
    echo($dir . $_REQUEST['id']);
  }elseif ($_REQUEST['action'] == 'editar') {
    echo($dir . $_REQUEST['oldid']);
    echo($dir . $_REQUEST['id']);
    rename($dir . $_REQUEST['oldid'], $dir . $_REQUEST['id']);
    $file = fopen($dir . $_REQUEST['id']."/input.txt","wb");
    fwrite($file,$_REQUEST['json'] );
    fclose($file);
  }
}

 ?>
